<?php
/**
 * Created by PhpStorm.
 * User: Игорь
 * Date: 24.12.2014
 * Time: 13:48
 */

use Symfony\Component\DomCrawler\Crawler;

class Parser {

    protected $extends = [];

    public function extend($name, $function)
    {
        $this->extends[$name] = $function;
    }

    /**
     * @param $xml
     * @param $model
     * @return array
     */
    public function fetch($xml, $model)
    {
        if ($xml instanceof MyCrawler) {
            $crawler = $xml;
        } else {
            $crawler = new MyCrawler($xml);
        }
        $out = [];
        foreach($model as $field=>$properties){
            $node = $crawler->filterXPath($properties['xpath']);
            $filters = $this->explodeFilters($properties['filter']);
            $value = $this->applyFilters($node,$filters);
            array_set($out,$field,$value);
        }
        return $out;
    }



    private function explodeFilters($filter)
    {
        $raw = explode('|', $filter);
        $filters = [];
        foreach ($raw as $row){
            if (strpos($row,':')!==false){
                list($rule,$params) = explode(':',$row,2);
                $params = str_getcsv($params,',');
                $filters[trim($rule)] = $params;
            } else {
                $filters[trim($row)] = [];
            }
        }
        return $filters;
    }

    /**
     * @param Crawler $node
     * @param $filters
     * @return mixed
     */
    private function applyFilters($node, $filters)
    {
        $parser = $this;
        $values = $node->each(function(MyCrawler $node)use($filters, $parser){
            $value = $node;
            foreach($filters as $filter=>$params){
                $value = $parser->applyFilter($value,$filter,$params);
            }
            return $value;
        });

        if (count($values) == 1){
            return array_pop($values);
        }

        if (count($values) == 0) return null;

        return $values;
    }

    public function applyFilter($value,$filter,$params){
        if (isset($this->extends[$filter])){
            return call_user_func($this->extends[$filter],$value,$params);
        }
        $method = 'filter'.studly_case($filter);
        return $this->$method($value,$params);
    }

    /**
     * @param Crawler $value
     * @return null|string
     */
    protected function filterText($value){
        if ($value->count() === 0){
            return null;
        }
        return $value->text();
    }

    /**
     * @param Crawler $value
     * @return null|string
     */
    protected function filterHtml($value){
        if ($value->count() === 0){
            return null;
        }
        return $value->html();
    }

    protected function filterBool($value){
        if (is_string($value)){
            $str = strtolower(trim($value));
            if ($str === 'true'){
                return true;
            }
            if ($str === 'false') {
                return false;
            }
        }
        return boolval($value);
    }

    /**
     * @param Crawler $value
     * @param $params
     * @return null|string
     */
    protected function filterAttr($value, $params){
        $attr = array_get($params,0);
        if (!$attr) return null;
        return $value->attr($attr);
    }

    /**
     * @param Crawler $value
     * @param $params
     * @return array|null
     */
    protected function filterModel($value, $params){
        $abstract = array_get($params,0);
        if (!$abstract) return null;
        $model = App::make($abstract);
        return $this->fetch($value,$model);
    }

    /**
     * @param $value
     * @param $params
     * @return bool
     */
    protected function filterRegexp($value,$params){
        $regexp = $params[0];
        $group = $params[1];
        if (preg_match($regexp,$value,$match)){
            return $match[$group];
        }
        return false;
    }

    /**
     * @param $value
     * @param $params
     * @return mixed
     */
    protected function filterRegexpReplace($value, $params){
        $regexp = $params[0];
        $replace = $params[1];
        return preg_replace($regexp,$replace,$value);
    }

    protected function filterTrim($value){
        return trim($value);
    }

}

class App {
    private static $container;
    public static function __callStatic($name, $arguments)
    {
        if (self::$container == null){
            self::$container = new \Illuminate\Container\Container();
        }
        return call_user_func_array([self::$container,$name],$arguments);
    }

}

class MyCrawler {
    protected $dom;
    protected $nodeList;
    protected $element;
    function __construct($xml)
    {
        if (is_string($xml)){
            $this->dom = new DOMDocument('1.0','UTF-8');
            $this->dom->loadXML($xml,LIBXML_NONET);
        }
        if ($xml instanceof DOMNodeList){
            $this->nodeList = $xml;
        }

        if ($xml instanceof DOMElement){
            $this->element = $xml;
        }
    }

    /**
     * @param $xpath
     * @return DOMNodeList
     */
    public function filterXPath($xpath)
    {
        $context = null;
        if ($this->dom){
            $dom = $this->dom;
        } elseif ($this->element) {
            $dom = $this->element->ownerDocument;
            $context = $this->element;
        }
        $DOMXpath = new DOMXPath($dom);
        $DOMXpath->registerNamespace('default','urn:yahoo:jp:auc:categoryTree');
        return new static($DOMXpath->query($xpath,$context));
    }

    public function each($closure){
        foreach ($this->nodeList as $node){
            $closure(new static($node));
        }
    }

    public function count(){
        if ($this->nodeList){
            return count($this->nodeList);
        }

        if ($this->element){
            return 1;
        }
        return 0;

    }

    public function text(){
        return $this->element->textContent;
    }
}