<?php

use Symfony\Component\DomCrawler\Crawler;

require_once "vendor/autoload.php";

require_once "Parser.php";

function fetchCategory($category_id){
    $xml = file_get_contents($category_id.'.xml');
    $crawler = new Crawler($xml);
    $child = $crawler->filterXPath('//default:CategoryId');
    $categories = [];
    $child->each(function(Crawler $node, $i)use(&$categories){
        $categories[] = $node->text();
    });
    return $categories;
}

function fetchCategoryWithParser($category_id){
    $xml = file_get_contents($category_id.'.xml');
    $parser = new Parser();
    $childModel = [
        'category_id' => [
            'xpath' => '//default:CategoryId',
            'filter' => 'text',
        ],
        'name' => [
            'xpath' => '//default:CategoryName',
            'filter' => 'text',
        ],
        'name_path' => [
            'xpath' => '//default:CategoryPath',
            'filter' => 'text',
        ],
        'path' => [
            'xpath' => '//default:CategoryIdPath',
            'filter' => 'text',
        ],
        'num_of_auctions' => [
            'xpath' => '//default:NumOfAuctions',
            'filter' => 'text',
        ],
        'parent' => [
            'xpath' => '//default:ParentCategoryId',
            'filter' => 'text',
        ],
        'is_leaf' => [
            'xpath' => '//default:IsLeaf',
            'filter' => 'text|bool',
        ],
        'depth' => [
            'xpath' => '//default:Depth',
            'filter' => 'text',
        ],
        'order' => [
            'xpath' => '//default:Order',
            'filter' => 'text',
        ],
        'is_link' => [
            'xpath' => '//default:IsLink',
            'filter' => 'text|bool',
        ],
        'is_leaf_to_link' => [
            'xpath' => '//default:IsLeafToLink',
            'filter' => 'text|bool',
        ]
    ];
    App::instance('ChildCategoryParserModel',$childModel);
    $model = [
        'name' => [
            'xpath' => '//default:Result/default:CategoryName',
            'filter' => 'text'
        ],
        'name_path' => [
            'xpath' => '//default:Result/default:CategoryPath',
            'filter' => 'text',
        ],
        'path' => [
            'xpath' => '//default:Result/default:CategoryIdPath',
            'filter' => 'text',
        ],

        'is_leaf' => [
            'xpath' => '//default:Result/default:IsLeaf',
            'filter' => 'text|bool',
        ],
        'depth' => [
            'xpath' => '//default:Result/default:Depth',
            'filter' => 'text',
        ],
        'order' => [
            'xpath' => '//default:Result/default:Order',
            'filter' => 'text',
        ],
        'is_link' => [
            'xpath' => '//default:Result/default:IsLink',
            'filter' => 'text|bool'
        ],
        'is_leaf_to_link' => [
            'xpath' => '//default:Result/default:IsLeafToLink',
            'filter' => 'text|bool'
        ],
        'child_category_num' => [
            'xpath' => '//default:Result/default:ChildCategoryNum',
            'filter' => 'text',
        ],
        'children' => [
            'xpath' => '//default:ChildCategory',
            'filter' => 'model:ChildCategoryParserModel'
        ]

    ];
    return $parser->fetch($xml,$model);
}

function fetchCategoryWithDOM($category_id){
    $xml = file_get_contents($category_id.'.xml');
    $dom = new DOMDocument('1.0','UTF-8');
//    $dom->validateOnParse = true;
    $dom->loadXML($xml,LIBXML_NONET);

    $xpath = new DOMXPath($dom);
    $xpath->registerNamespace('default','urn:yahoo:jp:auc:categoryTree');
    $domList = $xpath->query('//default:CategoryId');
    $ids = [];
    /** @var DOMElement $node */
    foreach($domList as $node){
        $ids[] = $node->textContent;
    }
    return $ids;
}

/**
 * @return array
 */
function test1()
{
    $a = memory_get_usage(true);
    $last = 0;
    echo "Wait\n";
    sleep(5);
    echo "Run test 1 \n";
    for ($i = 0; $i < 1000; $i++) {
        fetchCategory('0');
        $b = memory_get_usage(true);
        if ($last !== ($b - $a)) {
            $last = $b - $a;
            echo "Usage: $b, diff: $last \n";
        }
    }
}



echo "====================================\n";
function test2()
{
    echo "Wait\n";
    sleep(5);
    echo "Run test 2 \n";
    $a = memory_get_usage(true);
    $last = 0;
    for ($i = 0; $i < 100; $i++) {
        fetchCategoryWithParser('0');
        $b = memory_get_usage(true);
        if ($last !== ($b - $a)) {
            $last = $b - $a;
            echo "Usage: $b, diff: $last \n";
        }

    }
}

function test3(){
    echo "Wait\n";
    sleep(5);
    echo "Run test 3 \n";
    $a = memory_get_usage(true);
    $last = 0;
    for ($i = 0; $i < 10000; $i++) {
        fetchCategoryWithDOM('0');
//        print_r();
        $b = memory_get_usage(true);
        if ($last !== ($b - $a)) {
            $last = $b - $a;
            echo "Usage: $b, diff: $last \n";
        }

    }
}


test1();
test2();
test3();

